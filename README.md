# Notepad

This is a quick-n-dirty notepad that saves to your local storage. Nothing is sent to the Cloud. Bookmark this when you want to quickly take some notes.
